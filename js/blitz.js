/*################################
# FUNCTION AND CLASS DEFINITIONS #
################################*/

/**
 * BlitzLesenManager class:
 *   Reads and manages list of words,
 *   displays words,
 *   manages settings (i.e. font colors, font size and display time)
 */
class BlitzLesenManager {
    constructor(doc, settings, elements) {
        this.doc = doc;
        this.settings = settings;
        this.elements = elements;
        this.wordList = [];
        this.wordIdx = -1;
    }

    setCurrentWordIdxTracker() {
        this.elements.currentWordIndex.innerHTML = this.wordIdx + 1;
    }

    setDisplayTime(time) {
        console.log('change displayTime to ' + time);
        this.elements.displayTimeForm.value = time;  // update time form
        this.elements.displayTimeSlider.value = time;  // update time slider
        this.settings.displayTime = time;
    }

    setFontSize(size) {
        console.log('change fontSize to ' + size);
        this.elements.fontSizeForm.value = size;  // update font form
        this.elements.targetDiv.style.fontSize = size + 'pt';  // set target size
        this.settings.fontSize = size; // update settings 
    }

    setFontColor(colorArray) {
        console.log('change colorArray to ' + colorArray)
        this.settings.fontColors = colorArray;  // update settings
        this.elements.fontColorSelector1.value = colorArray[0];  // update selector 1
        this.elements.fontColorSelector2.value = colorArray[1];  // update selector 2
    }

    /**
     * Parse contents of a new text file
     * @param {string} text - content of the input text file
     */
    loadNewWordList(text) {
        var inputList = text.split(/\r?\n/).filter(Boolean);  // split lines

        // parse input; differentiate between options and content
        var optionWords = [];
        var contentWords = [];
        for (let i = 0; i < inputList.length; i++) {
            var item = inputList[i].trim();
            if (item.startsWith('@')) {
                // parse options
                console.log('parse option ' + item + ' on line ' + i);
                optionWords.push(item);
            } else if (item.startsWith('-')) {
                console.log('pass separator on line ' + i);
                // pass separator lines
            } else {
                // use all other as wordListe entries
                contentWords.push(item);
            }
        }

        if (optionWords.length > 0) {
            // set settings from file
            this.setFileParameters(optionWords);
        }

        this.wordList = contentWords;
        this.wordIdx = -1;  // reset
        totalWordCount.innerHTML = this.wordList.length;
        this.setCurrentWordIdxTracker();
        console.log('set new word list:')
        console.log(this.wordList)
    }

    /**
     * Set options from input file
     * @param {array} optionList - list of option lines from input file
     */
    setFileParameters(optionList) {
        for (let i = 0; i < optionList.length; i++) {
            var optionStr = optionList[i];
            var [param, value] = optionStr.split(':');
            param = param.toLowerCase().trim();
            value = value.toLowerCase().trim();
            switch (param) {
                case '@color':
                    this.setFontColor([value, value])
                    break;
                case '@color1':
                    this.setFontColor([this.settings.fontColors[0], value])
                    break;
                case '@color2':
                    this.setFontColor([value, this.settings.fontColors[1]])
                    break;
                case '@displaytime':
                    this.setDisplayTime(value);
                    break;
                case '@fontsize':
                    this.setFontSize(value);
                    break;
                default:
                    console.log('unknown parameter ' + param);
            }
        }
    }

    /**
     * display word for current this.wordIdx
     */
    showCurrentWord() {
        // for initial word displays: default to idx 0
        if (this.wordIdx == -1) {
            this.wordIdx = 0;
        }

        // display word at pos this.wordIdx
        if (this.wordList.length > 0) {
            let word = this.wordList[this.wordIdx]
            this.showWordSyllables(word);  // display word
        } else {
            console.log('no input text array')
        }
        this.setCurrentWordIdxTracker();  // update idx display
    }

    /**
     * advance to next word and display
     */
    nextWord() {
        // update index
        if (this.wordIdx < this.wordList.length - 1) {
            this.wordIdx += 1;
        } else { this.wordIdx = 0; }
        // update display
        this.elements.currentWordIndex.innerHTML = this.wordIdx;
        // display word
        this.showCurrentWord();
    }

    /**
     * go back to last word and display
     */
    previousWord() {
        // update index
        if (this.wordIdx > 0) {
            this.wordIdx -= 1;
        } else { this.wordIdx = this.wordList.length - 1; }
        // update display
        this.elements.currentWordIndex.innerHTML = this.wordIdx;
        // display word
        this.showCurrentWord();
    }

    /**
     * display word given as string
     * 
     * word can contain multiple syllables (with delimiter '\'),
     * subsequent syllables are displayed in different colors
     * (if settings.fontColors contains more than one color)
     * 
     * @param {string} word - word to be displayed
     */
    showWordSyllables(word) {
        let targetDiv = this.elements.targetDiv;
        // store placeholder in text & clean
        targetDiv.innerHTML = '';

        // split and iterate through syllables
        var split_word = word.split('\\')
        for (let i = 0; i < split_word.length; i++) {

            // select color for syllable
            var color_idx = i % this.settings.fontColors.length
            var color = this.settings.fontColors[color_idx]
            var syl = split_word[i]

            // make new span with syllable and color
            var wordSpan = document.createElement('span')
            wordSpan.innerHTML = syl;
            wordSpan.style.color = color;

            //append to target div
            targetDiv.appendChild(wordSpan);
        }

        //after displayTime has passed:
        setTimeout(() => {
            // restore placeholder
            this.resetPrompt();
        }, this.settings.displayTime);
    }

    /**
     * reset prompt (after showWordSyllables)
     */
    resetPrompt() {
        // reset fixation character ('×')
        this.elements.targetDiv.innerHTML = this.settings.fixationChar;
        // reset color (black)
        this.elements.targetDiv.style.color = this.settings.fixationColor;
    }
}

/**
 * Handle navigation overlay
 */
var overlayVisible = false;
function openNav() {
    document.getElementById("myNav").style.width = "100%";
    $("#MenuButton").hide()
    overlayVisible = true;
}
function closeNav() {
    document.getElementById("myNav").style.width = "0%";
    $("#MenuButton").show()
    overlayVisible = false;
}

/*######
# MAIN #
######*/

document.addEventListener('DOMContentLoaded', function () {

    // collect elements
    const displayTimeSlider = document.getElementById("displayTimeSlider");
    const displayTimeForm = document.getElementById("displayTimeForm");
    const fontSizeForm = document.getElementById("fontSizeForm");
    const targetDiv = document.getElementById("target");
    const currentWordIndex = document.getElementById("currentWordIndex");
    const totalWordCount = document.getElementById("totalWordCount");
    const inputFileUpload = document.getElementById('inputFileUpload');
    const fontColorSelector1 = document.getElementById('fontColorSelector1')
    const fontColorSelector2 = document.getElementById('fontColorSelector2')

    // bundle in object
    const elements = {
        displayTimeSlider: displayTimeSlider,
        displayTimeForm: displayTimeForm,
        fontSizeForm: fontSizeForm,
        targetDiv: targetDiv,
        currentWordIndex: currentWordIndex,
        totalWordCount: totalWordCount,
        inputFileUpload: inputFileUpload,
        fontColorSelector1: fontColorSelector1,
        fontColorSelector2: fontColorSelector2
    }

    // get initial settings from elements
    const settings = {
        fontColors: [fontColorSelector1.value, fontColorSelector2.value],
        displayTime: displayTimeSlider.value,
        fontSize: fontSizeForm.value,
        fixationChar: '×',
        fixationColor: 'black'
    }

    // init manager
    targetDiv.style.fontSize = fontSizeForm.value + 'pt';
    manager = new BlitzLesenManager(document, settings, elements)


    /*######################
    # EVENT LISTENER SETUP #
    ######################*/

    /* File Input */

    inputFileUpload.addEventListener('change', function () {

        let fr = new FileReader();
        fr.onload = function () {
            manager.loadNewWordList(fr.result)
        }
        // read input file as UTF8
        fr.readAsText(this.files[0]);

    })

    /* Slider and Forms */

    // Update time slider
    displayTimeSlider.addEventListener("change", function () {
        manager.setDisplayTime(this.value);
    })

    // Update time form
    displayTimeForm.addEventListener("input", function () {
        manager.setDisplayTime(this.value);
    });

    // Update font form
    fontSizeForm.addEventListener("input", function () {
        manager.setFontSize(this.value);
    });

    // Update font color forms
    fontColorSelector1.addEventListener("change", function () {
        manager.setFontColor([this.value, fontColorSelector2.value]);
    });
    fontColorSelector2.addEventListener("change", function () {
        manager.setFontColor([fontColorSelector1.value, this.value]);
    });

    /* Buttons */

    // show word on click
    $("#showButton").click(function () {
        manager.showCurrentWord();
    });

    // toggle settings div
    $("#settingsToggle").click(function () {
        $("#settingsContainer").toggle();
    });

    // next word
    $("#nextImageButton").click(function () {
        manager.nextWord();
    });

    // previous image
    $("#prevImageButton").click(function () {
        manager.previousWord();
    });

    /* Keys */

    document.addEventListener('keydown', (event) => {

        switch (event.key) {
            case 'ArrowLeft':
                manager.previousWord();
                break;
            case 'ArrowRight':
                manager.nextWord();
                break;
            case 'ArrowUp':
            case 'ArrowDown':
            case ' ':
                manager.showCurrentWord();
                break;
            case 'i':
            case 'I':
                if (overlayVisible) {
                    closeNav();
                } else {
                    openNav();
                }
                break;
            case 'Escape':
                closeNav();
                break;
            case '+':
                fontSize = manager.settings.fontSize;
                manager.setFontSize(fontSize + 5);
                break;
            case '-':
                fontSize = manager.settings.fontSize;
                manager.setFontSize(fontSize - 5);
                break;
            default:
                console.log('unassigned key: ' + event.key)
        }

    }, false);

});